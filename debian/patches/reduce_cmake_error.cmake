Description: Reduce the error message in cmake if module is missing
Author: Anton Gladky <gladk@debian.org>
Bug-Debian: https://bugs.debian.org/783797
Last-Update: 2015-05-05

Index: ParaView-v5.4.1/VTK/CMake/vtkModuleAPI.cmake
===================================================================
--- ParaView-v5.4.1.orig/VTK/CMake/vtkModuleAPI.cmake
+++ ParaView-v5.4.1/VTK/CMake/vtkModuleAPI.cmake
@@ -50,7 +50,7 @@ macro(vtk_module_load mod)
       include(${mod} OPTIONAL)
     endif()
     if(NOT ${mod}_LOADED)
-      message(FATAL_ERROR "No such module: \"${mod}\"")
+      message(STATUS "No such module: \"${mod}\"")
     endif()
   endif()
 endmacro()
@@ -110,14 +110,11 @@ endmacro()
 macro(vtk_module_config ns)
   set(_${ns}_MISSING ${ARGN})
   if(_${ns}_MISSING)
-    list(REMOVE_ITEM _${ns}_MISSING ${VTK_MODULES_ENABLED})
-  endif()
-  if(_${ns}_MISSING)
     set(msg "")
     foreach(mod ${_${ns}_MISSING})
       set(msg "${msg}\n  ${mod}")
     endforeach()
-    message(FATAL_ERROR "Requested modules not available:${msg}")
+    message(STATUS "Requested modules not available:${msg}")
   endif()
 
   set(${ns}_DEFINITIONS "")
