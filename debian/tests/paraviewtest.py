#! /usr/bin/env python

# Simple autotest to check Python-Paraview

from paraview.simple import *
cone = Cone()
cone.Resolution
cone.Resolution = 32
cone = Cone(Resolution=32)
cone.Center
cone.Center = [1, 2, 3]
cone.Center[0:2] = [2, 4]
cone.Center
shrinkFilter = Shrink(cone)
shrinkFilter.Input
shrinkFilter.UpdatePipeline()
shrinkFilter.GetDataInformation().GetNumberOfCells()
shrinkFilter.GetDataInformation().GetNumberOfPoints()
